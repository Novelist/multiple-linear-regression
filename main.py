import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

# check if a given list only contains numerical values
def check_for_numerical(list):
    pass

# convert a non-numerical list to a numerical list
def convert_to_numerical(list):
    pass

# import dataset
dataset = pd.read_csv("insurance.csv")

# pre-processing data
print(dataset.shape)
print(dataset.isna().sum())
print(dataset.duplicated().any())

# replace categorial to numerical values
# sex: female = 0 / male = 1 - in this case, as there are only two genders available in the dataset, using them as a binary variable is suitable
print(np.unique(np.array(dataset["sex"])))
dataset["sex"].replace(["female","male"],[0,1], inplace=True)

# smoker: yes = 1 / no = 0
print(np.unique(np.array(dataset["smoker"])))
dataset["smoker"].replace(["no","yes"],[0,1], inplace=True)

#region: one hot encoding (OHE)
print(np.unique(np.array(dataset["region"])))
dataset = pd.get_dummies(dataset, columns=["region"])
#dataset["region"].replace(["northeast","southeast","southwest","northwest"],[1,2,3,4], inplace=True)

print(dataset.head())

x = dataset[["age","sex","bmi","children","smoker","region_northeast","region_northwest","region_southeast","region_southwest"]]
y = dataset[["charges"]]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.3, random_state = 42)

mlr = LinearRegression()
mlr.fit(x_train, y_train)
print("Intercept: ", mlr.intercept_)
print("Coefficients:")
coef = [round(coefficient,2) for coefficient in mlr.coef_[0]]
print(list(zip(x,coef)))