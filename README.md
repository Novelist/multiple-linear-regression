## Multiple Linear Regression

Based on the portfolio project on Codecademy, this project handles a dataset of U.S. medical insurance data.
For further information, open the Jupyter Notebook for detailed description of the dataset.